package es.palacios;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;


public class Escacs extends Application implements EventHandler<MouseEvent>{

    public final static int WIDTH = 50;

    private Label indicatorLabel;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Tasca 1.1 Escacs");
        Group board = new Group();

        for (int x = 1; x < 11; x++) {
            for (int y = 1; y < 11; y++) {
                Rectangle rectangle = new Rectangle(WIDTH,WIDTH);
                rectangle.setX(x * WIDTH);
                rectangle.setY(y * WIDTH);
                rectangle.setId("Has clicat la posició (" + x + "," + y + ")");
                if ((x + y) % 2 == 0) {
                    rectangle.setFill(Color.rgb(210,255,51));
                } else {
                    rectangle.setFill(Color.rgb(227,51,255));
                }
                rectangle.setOnMouseClicked(this);
                board.getChildren().add(rectangle);
            }
        }
        indicatorLabel = new Label("Res presionat");
        indicatorLabel.setPadding(new Insets(20,20,20,20));
        VBox layout = new VBox();
        layout.setAlignment(Pos.TOP_CENTER);
        layout.getChildren().addAll(board,indicatorLabel);

        Scene theScene = new Scene( layout, 500, 550);
        stage.setScene( theScene );
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        Rectangle rectangle = (Rectangle) mouseEvent.getSource();
        indicatorLabel.setText(rectangle.getId());
    }
}