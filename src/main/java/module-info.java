module es.palacios {
    requires javafx.controls;
    requires javafx.fxml;


    opens es.palacios to javafx.fxml;
    exports es.palacios;
}